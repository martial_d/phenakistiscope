import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk

"""
"Méta-widget" pour afficher une image ou une animation avec la présence de barres de défilement 
permettant d'afficher une image plus grande que le widget parent. Il est possible d'appliquer un rognage à l'image 
ainsi que de définir une échelle d'affichage permettant de redimensioner l'image. Hérite de la classe tk.Frame.
"""
class ScrollableImage(tk.Frame):
    """
    Constructeur.
    """
    def __init__(self, master=None, **kw):
        # Initialiser la frame
        super().__init__(master=master, **kw)

        self.__imagesPIL = []                   # images PIL.Image
        self.__images = []                        # images PIL.ImageTk.PhotoImage (nécessaire de les garder en mémoire pour assurer l'affichage) 
        self.__images_canvas = []                 # images dessinées dans le canvas
        self.__index_img = 0                    # indice de l'image courante dans les tableaux
        self.__canvas = None                    # canvas contenant l'image affichée
        self.__parent = None                    # widget parent contenant le canvas 
        self.__size_canvas_default = (200,200)  # taille du canvas par défaut
        self.__scrollbar_h = None                 # Barre de défilement horizontale
        self.__scrollbar_v = None                 # Barre de défilement verticale
        self.__size_scale = 1.0                 # échelle d'affichage
        self.__auto_fit = True                  # ajustement automatique de la taille à la taille du widget parent
        self.__crop_bbox = (0,0,0,0)            # boite englobante définissant l'image rognée
        self.__size_cropped = (0,0)             # taille de l'image rognée
        self.__refresh_delay = None             # durée d'affichage d'une image
        self.__after_id_refresh = None          # identifiant de l'appel à la fonction pour d'actualisation
        self.__after_id_resize = None           # identifiant de l'appel à la fonction pour d'actualisation

        self.set_fps(10)
        self._create_widgets()

    """
    Création et ajout des widgets qui compose ce "méta-widget"
    """
    def _create_widgets(self, parent=None):
        if parent == None:
            parent = self
        self.__parent = parent
        self.__parent.bind("<Configure>", self.__onresize)
        # Canvas contenant l'image
        self.__canvas = tk.Canvas(parent, width = self.__size_canvas_default[0], height = self.__size_canvas_default[1])
        # Création des deux scrollbars vertical et horrizontal
        self.__scrollbar_v = ttk.Scrollbar(parent, orient="vertical")
        self.__scrollbar_h = ttk.Scrollbar(parent, orient="horizontal")
        # Ajout et positionnement dans la frame
        self.__scrollbar_v.pack(side="right", fill="y")
        self.__scrollbar_h.pack(side="bottom", fill="x")    
        self.__canvas.pack(side="left", fill=tk.BOTH, expand=True) 
        # Lier les scrollbars au canvas
        self.__canvas.config(xscrollcommand=self.__scrollbar_h.set, yscrollcommand=self.__scrollbar_v.set)
        # Lier la zone d'affichage du canvas aux scrollbars
        self.__scrollbar_v.config(command=self.__canvas.yview)
        self.__scrollbar_h.config(command=self.__canvas.xview)
        # Définir et mettre à jour la taille de la zone pour le défilement
        self.__canvas.config(scrollregion=self.__canvas.bbox('all'))

    """
    Changer l'image à afficher. L'image peut aussi être une "image animée".
    imgsPIL : tableau d'images comprenant une ou plusieurs images de même dimensions. 
        Si plusieurs images sont présentes alors elles seront traitées comme étant une animation et l'animation sera lu directement.
    size_scale : échelle d'affichage de l'image entre 0 et 1
    crop_bbox : boite englobante permettant de rogner l'image. Elle indique la partie de l'image à conserver.
    """
    def set_images(self, imgsPIL, size_scale=None, crop_bbox=None):
         # vider les images actuellement présentes
        self.clear()
        # redéfinir l'échelle et le rognage
        if crop_bbox == None:
            crop_bbox = (0,0,imgsPIL[0].size[0],imgsPIL[0].size[1])
        self.__imagesPIL = imgsPIL
        self.set_crop(crop_bbox, False)
        if size_scale == None:
            if self.__auto_fit:
                self.set_size_scale(-1, False)
        else:
            self.__size_scale = size_scale
        
        self.__update_images()

    """
    Renvoie la liste des images
    """
    def get_images(self):
        return self.__imagesPIL

    """
    Permet d'activer ou désactiver l'ajustement automatique de la taille par rapport à l'élément parent
    fit : une valeur à "True" permet d'activer l'ajustement auto et "False" de le désactiver
    """
    def set_auto_fit(self, fit=True):
        self.__auto_fit = fit
        self.__update_images(True)

    """
    Modifier le paramètre de redimensionnement. Valeur en pourcentage [0; 1.0] par rapport à la taille de l'image d'origine.
    size_scale : Si size_scale < 0 alors la taille est calculée de manière à être ajustée à la taille du widget
    """
    def set_size_scale(self, size_scale, show=True):
        if size_scale >= 0:
            self.__size_scale = size_scale
        else:
            size_scale = 1.0
            frame_max_size = (self.__parent.winfo_width() - self.__scrollbar_v.winfo_width()-4, self.__parent.winfo_height() - self.__scrollbar_h.winfo_height()-4)

            if self.__size_cropped[0] > frame_max_size[0] or self.__size_cropped[1] > frame_max_size[1]:
                size_scale_tmp0 = frame_max_size[0]/self.__size_cropped[0]
                size_scale_tmp1 = frame_max_size[1]/self.__size_cropped[1]

                if size_scale_tmp0 < size_scale_tmp1:
                    size_scale = size_scale_tmp0
                else:
                    size_scale = size_scale_tmp1

            self.__size_scale = size_scale
        if show:
            self.__update_images()
        return size_scale

    """
    Renvoie l'échelle actuelle de l'image
    """
    def get_size_scale(self):
        return self.__size_scale

    """
    Permet de définir le rognage à appliquer à l'image
    crop_bbox : boite englobante définnissant la zone de l'image à conserver
    """
    def set_crop(self, crop_bbox, show=True):
        self.__crop_bbox = crop_bbox
        self.__size_cropped = (self.__crop_bbox[2] - self.__crop_bbox[0], self.__crop_bbox[3] - self.__crop_bbox[1])
        if show:
            self.__update_images()

    """
    Définir la durée d'affichage d'une image en fonction de la fréquence d'affichage
    fps : nombre d'images par seconde
    """
    def set_fps(self, fps=10):
        self.__refresh_delay = round(1000/fps)

    """
    Renvoie la fréquence d'affichage actuelle
    """
    def get_fps(self):
        return round(1000/self.__refresh_delay)

    """
    Reprendre la lecture d'une animation
    """
    def animation_play(self):
        if self.__images_canvas > 1:
            self.__refresh()

    """
    Arrêter la lecture d'une animation
    """
    def animation_pause(self):
        if self.__after_id_refresh:
            self.__canvas.after_cancel(self.__after_id_refresh)
            self.__after_id_refresh = None

    """
    Vider le canvas de toutes ses images et le réinitialiser
    """
    def clear_canvas(self):
        self.__images = []
        self.__images_canvas = []
        self.__index_img = 0
        self.__canvas.delete("all")
        self.__canvas.configure(width = self.__size_canvas_default[0], height = self.__size_canvas_default[1])
        self.__canvas.config(scrollregion=self.__canvas.bbox('all'))
        if self.__after_id_refresh:
            self.__canvas.after_cancel(self.__after_id_refresh)
            self.__after_id_refresh = None

    """
    Vider et effacer les images
    """
    def clear(self):
        self.clear_canvas()
        self.__imagesPIL = []
    
    """
    Vider les images et remettre à zéro l'échelle et le rognage 
    """
    def clear_deep(self):
        self.clear()
        self.__size_scale = 1.0
        self.__auto_fit = True
        self.__crop_bbox = (0,0,0,0)
        self.__size_cropped = (0,0)
        self.__refresh_delay = None

    """
    Fonction permettant de dessiner et ajouter les images dans le canvas
    calc_fit : Permet de recalculer automatiquement l'échelle d'affichage, si la valeur vaut "True" et que l'ajustement auto est actif.
    """
    def __update_images(self, calc_fit=False):
        if calc_fit and self.__auto_fit:
            self.set_size_scale(-1)
        self.clear_canvas()
        for img in self.__imagesPIL:
            im = img.crop(self.__crop_bbox)
            im = im.resize((round(im.size[0]*self.__size_scale), round(im.size[1]*self.__size_scale)))
            self.__images.append(ImageTk.PhotoImage(im))
            id = self.__canvas.create_image(0, 0, anchor='nw', image=self.__images[len(self.__images)-1])
            self.__images_canvas.append(id)
        self.__canvas.config(scrollregion=self.__canvas.bbox('all'))

        for img in self.__images_canvas:
            self.__canvas.itemconfigure(img, state='hidden')

        if len(self.__imagesPIL) > 1:
            self.__refresh()
        else:
            self.__show()

    """
    Affiche l'image qui est indiqué par l'indice en mémoire
    """
    def __show(self):
        if len(self.__images_canvas) > 0:
            prev_index = self.__index_img -1
            if prev_index < 0:
                prev_index = len(self.__images)-1
            self.__canvas.itemconfigure(self.__images_canvas[prev_index], state='hidden')
            self.__canvas.itemconfigure(self.__images_canvas[self.__index_img], state='normal')

    """
    Fonction appelée à intervalle régulier pour passer d'une image à une autre dans le cas d'une animation
    """
    def __refresh(self):
        self.__show()
        if self.__index_img < len(self.__imagesPIL)-1:
            self.__index_img += 1
        else:
            self.__index_img = 0
        # request tkinter to call self.refresh after 1s (the delay is given in ms)
        if self.__refresh_delay:
            self.__after_id_refresh = self.__canvas.after(self.__refresh_delay, self.__refresh)
    
    """
    Fonction appelée lorsque la taille du widget parent change
    """
    def __onresize(self, event):
        if self.__auto_fit: # si l'ajustement auto est activé
            # supprimer l'ancienne demande d'actualisation si elle existe
            if self.__after_id_resize:
                self.__canvas.after_cancel(self.__after_id_resize)
                self.__after_id_resize = None
            # créer une nouvelle demande d'actualisation dans 150 ms
            self.__after_id_resize = self.__canvas.after(150, lambda: self.__update_images(True))

    

    
"""
"Méta-widget" pour afficher une image ou une animation avec la présence de barres de défilement 
permettant d'afficher une image plus grande que le widget parent ainsi que de widgets de contrôles permettant 
d'ajuster la taille d'affichage de l'image. Hérite de la classe ScrollableImage.
"""
class ScrollableImageControls(ScrollableImage):
    def __init__(self, master=None, **kw):
        super().__init__(master=master, **kw)
        
    """
    Redéfinition de la méthode "_create_widget" de la classe parente.
    Création et initialisation des widgets
    """
    def _create_widgets(self):
        self.__frame_main = tk.Frame(self)
        super()._create_widgets(self.__frame_main)

        self.__frame_controls = tk.Frame(self)
        self.__frame_controls.pack(side=tk.TOP, fill=tk.BOTH)
        self.__frame_main.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)


        self.__label_scale = tk.Label(self.__frame_controls, text="Échelle %")
        self.__label_scale.pack(side=tk.LEFT, anchor=tk.S)

        self.__scale = tk.Scale(self.__frame_controls, orient="horizontal", from_=10, to=100, resolution=1, tickinterval=0, length=200, state="disabled")
        self.__scale.pack(side=tk.LEFT, anchor=tk.S)

        self.__btn_fit = tk.Button(self.__frame_controls, text="Ajuster", state="disabled", command= self.__onclick_btn_fit)
        self.__btn_fit.pack(side=tk.LEFT, anchor=tk.S)

        self.__checkvar_fit = tk.IntVar()
        self.__checkvar_fit.set(1)
        self.__checkbtn_fit = tk.Checkbutton(self.__frame_controls, text="Ajustement auto.", variable=self.__checkvar_fit, command=self.__onchange_checkbtn_fit)
        self.__checkbtn_fit.pack(side=tk.LEFT, anchor=tk.S)

    """
    Fonction appelée lors d'un clic sur le bouton d'ajustement de la taille 
    """
    def __onclick_btn_fit(self):
        self.set_size_scale(-1)
        self.__scale.set(round(self.get_size_scale()*100))

    """
    Fonction appelée lors d'un changement sur la case à cocher pour l'activation/désactivation de l'ajustement auto
    """
    def __onchange_checkbtn_fit(self):
        self.set_auto_fit(self.__checkvar_fit.get() == 1)
        if self.__checkvar_fit.get() == 0:
            self.__scale.configure(state="normal")
            self.__btn_fit.configure(state="normal")
            self.__bind_id_scale = self.__scale.bind("<ButtonRelease-1>", lambda event: self.set_size_scale(self.__scale.get()/100))
            self.set_size_scale(self.__scale.get()/100)
        else:
            self.__scale.configure(state="disabled")
            self.__btn_fit.configure(state="disabled")
            self.__scale.unbind("<ButtonRelease-1>", self.__bind_id_scale)