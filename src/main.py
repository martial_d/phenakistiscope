"""
*** Phénakitiscope ***

Cette application permet de générer des disques sous la forme d'images qui contiennent eux-même 
une séquence d'images représentant une animation. 
Le but est de créer des disques à imprimer pouvant être utilisé pour la réalisation de Phénakitiscope.
Il est également possible de l'utiliser à des fins expérimentales et artistiques et créer ainsi des motifs originaux. 

Auteur : Martial D.
Date : 03/2021
Version : 1.0.0
"""

import tkinter as tk
from tkinter import ttk, filedialog, messagebox, colorchooser
from PIL import Image
import pathlib

import phenakitiscope
import scrollableimage as si


"""
Classe principale du programme
"""
class Application():
    """
    Constructeur de la classe. Déclaration et affectation des variables de base
    """
    def __init__(self):
        self.filenames = []
        # Taille maximale de l'image générée  
        self.max_output_size = 1000
        # Marges entre le dessin et les bords de l'image en pourcentage 
        self.output_margin_coeff = 2/100

        self.selected_input_img = None

        self.__phenakitiscope = phenakitiscope.Phénakistiscope()


    """
    Point d'entrée du programme. Création de la fenêtre principale.
    """
    def main(self):
        # Création de la fenêtre racine
        self.__root = tk.Tk()
        # Titre de la fenêtre
        self.__root.title("Phénakistiscope")

        # Bloc pour la selection des fichiers
        self.__frame_files = tk.Frame(self.__root)
        self.__frame_files.pack(side="top", fill="both")

        self.__frame_files_list = tk.Frame(self.__frame_files)
        self.__frame_files_list.pack(side="left", fill="x", expand=True)

        self.__frame_files_controls = tk.Frame(self.__frame_files)
        self.__frame_files_controls.pack(side="left", anchor="w")


        # Widgets pour la selection des images
        self.__stringvar_files_nb = tk.StringVar()
        self.__stringvar_files_nb.set("Aucune image selectionnée")
        self.__label_files_nb = tk.Label(self.__frame_files_controls, textvariable=self.__stringvar_files_nb)
        self.__label_files_nb.pack(side="top", anchor="center")
        self.__btn_browse_files = tk.Button(self.__frame_files_controls, text="Parcourir...", command=self.__select_files)
        self.__btn_browse_files.pack(side="top",  anchor="center")

        # Widgets pour afficher la liste des fichiers
        self.__tree_files = ttk.Treeview(self.__frame_files_list, columns=('path', 'size'), selectmode="browse")
        self.__tree_files.column("#0", width = 50, minwidth = 50, stretch = False, anchor = "center")
        self.__tree_files.column("path", width = 400, minwidth = 100, stretch = False, anchor = "w")
        self.__tree_files.column("size", width = 100, minwidth = 75, stretch = False, anchor = "w")
        self.__tree_files.heading("#0", text = "Ordre", anchor = "center")
        self.__tree_files.heading("path", text = "Fichier", anchor = "w")
        self.__tree_files.heading("size", text = "Taille", anchor = "w")
        self.__tree_files.bind('<<TreeviewSelect>>', self.__show_input_img)

        self.__scrollbar_v_tree = ttk.Scrollbar(self.__frame_files_list, orient="vertical", command=self.__tree_files.yview)
        self.__tree_files.configure(yscrollcommand=self.__scrollbar_v_tree.set)
        
        self.__tree_files.pack(side="left", fill="both", expand=True)
        self.__scrollbar_v_tree.pack(side="right", fill="y")

        # Bloc principal dédié aux réglage et à l'affichage des images
        self.__frame_img_global = tk.Frame(self.__root)
        self.__frame_img_global.pack(side="bottom", fill="both", expand=True)

        # Bloc pour les paramètres
        self.__frame_parameters = tk.Frame(self.__frame_img_global)
        self.__frame_parameters.pack(side="top", fill="both")

        # Paramètres pour les images sources
        self.__frame_in_parameters = tk.LabelFrame(self.__frame_parameters, text="Ajuster les images de la séquence")
        self.__frame_in_parameters.pack(side="left", fill="both", expand=True)

        self.__text_size_in = tk.StringVar()
        self.__print_size(self.__text_size_in)
        self.__label_size_in = tk.Label(self.__frame_in_parameters, textvariable=self.__text_size_in)
        self.__label_size_in.pack(side="top", anchor="w")

        self.__frame_crop_h = tk.Frame(self.__frame_in_parameters)
        self.__frame_crop_h.pack(side="top", anchor="w")
        self.__label_crop_h = tk.Label(self.__frame_crop_h, text="Rogner horizontalement (%)")
        self.__label_crop_h.pack(side="left", anchor="s")
        self.__scale_crop_h = tk.Scale(self.__frame_crop_h, orient="horizontal", from_=0, to=90, resolution=1, tickinterval=0, length=200)
        self.__scale_crop_h.pack(side="right")

        self.__frame_crop_v = tk.Frame(self.__frame_in_parameters)
        self.__frame_crop_v.pack(side="top", anchor="w")
        self.__label_crop_v = tk.Label(self.__frame_crop_v, text="Rogner verticalement (%)")
        self.__label_crop_v.pack(side="left", anchor="s")
        self.__scale_crop_v = tk.Scale(self.__frame_crop_v, orient="horizontal", from_=0, to=90, resolution=1, tickinterval=0, length=200)
        self.__scale_crop_v.pack(side="right")
        self.__scale_crop_h.bind("<ButtonRelease-1>", self.__onchange_crop)
        self.__scale_crop_v.bind("<ButtonRelease-1>", self.__onchange_crop)


        # Paramètres pour l'image en sortie
        self.__frame_out_parameters = tk.LabelFrame(self.__frame_parameters, text="Réglages du rendu")
        self.__frame_out_parameters.pack(side="right", fill="both", expand=True)

        self.__text_size_out = tk.StringVar()
        self.__print_size(self.__text_size_out)
        self.__label_size_out = tk.Label(self.__frame_out_parameters, textvariable=self.__text_size_out)
        self.__label_size_out.grid(column=0, row=0, columnspan=5, sticky="W")

        # Widgets pour choisir le mode d'integration des images
        self.__label_margins = tk.Label(self.__frame_out_parameters, text="Positionnement des images")
        self.__label_margins.grid(column=0, row=1, columnspan=2, sticky="W")

        self.__frame_images = tk.Frame(self.__frame_img_global)
        self.__frame_images.pack(side="bottom", fill="both", expand=True)

        f_validation = self.__root.register (self.__validate)

        self.__stringvar_margin_top = tk.StringVar() 
        self.__stringvar_margin_top.trace_add('write', self.__margin_changed)
        self.__spinbox_top = ttk.Spinbox(self.__frame_out_parameters, width=5, from_=0, to=99999, textvariable=self.__stringvar_margin_top) 
        self.__spinbox_top.config(validate ="key", validatecommand =(f_validation, "%P", "%W")) 
        self.__spinbox_top.grid(column=1, row=2)

        self.__stringvar_margin_left = tk.StringVar() 
        self.__stringvar_margin_left.trace_add('write', self.__margin_changed)
        self.__spinbox_left = ttk.Spinbox(self.__frame_out_parameters, width=5, from_=0, to=99999, textvariable=self.__stringvar_margin_left)
        self.__spinbox_left.config(validate ="key", validatecommand =(f_validation, "%P", "%W")) 
        self.__spinbox_left.grid(column=0, row=3)

        self.__stringvar_margin_right = tk.StringVar() 
        self.__stringvar_margin_right.trace_add('write', self.__margin_changed)
        self.__spinbox_right = ttk.Spinbox(self.__frame_out_parameters, width=5, from_=0, to=99999, textvariable=self.__stringvar_margin_right)
        self.__spinbox_right.config(validate ="key", validatecommand =(f_validation, "%P", "%W")) 
        self.__spinbox_right.grid(column=2, row=3)

        self.__stringvar_margin_bottom = tk.StringVar() 
        self.__stringvar_margin_bottom.trace_add('write', self.__margin_changed)
        self.__spinbox_bottom = ttk.Spinbox(self.__frame_out_parameters, width=5, from_=0, to=99999, textvariable=self.__stringvar_margin_bottom)
        self.__spinbox_bottom.config(validate ="key", validatecommand =(f_validation, "%P", "%W")) 
        self.__spinbox_bottom.grid(column=1, row=4)

        self.__spinbox_top.set(0)
        self.__spinbox_left.set(0)
        self.__spinbox_right.set(0)
        self.__spinbox_bottom.set(0)

        self.__btn_fill = tk.Button(self.__frame_out_parameters, text="Remplir", command=lambda: self.__set_margin(0), state="disabled")
        self.__btn_insert = tk.Button(self.__frame_out_parameters, text="Insérer", command=lambda: self.__set_margin(1), state="disabled")
        self.__btn_fill.grid(column=3, row=3)
        self.__btn_insert.grid(column=4, row=3)

        self.__checkvar_background = tk.IntVar()
        self.__checkbtn_background = tk.Checkbutton(self.__frame_out_parameters, text="Fond", variable=self.__checkvar_background, command=lambda: self.__draw_part(self.__btn_background_color, self.__checkvar_background))
        self.__checkbtn_background.grid(column=5, row=0, sticky="W")
        self.__btn_background_color = tk.Button(self.__frame_out_parameters, width = 5, bg="#000", command= lambda: self.__define_color(self.__btn_background_color, self.__checkvar_background))
        self.__btn_background_color.grid(column=6, row=0)

        self.__checkvar_outlines = tk.IntVar()
        self.__checkbtn_outlines = tk.Checkbutton(self.__frame_out_parameters, text="Contours", variable=self.__checkvar_outlines, command=lambda: self.__draw_part(self.__btn_outlines_color, self.__checkvar_outlines))
        self.__checkbtn_outlines.grid(column=5, row=1, sticky="W")
        self.__btn_outlines_color = tk.Button(self.__frame_out_parameters, width = 5, bg="#000", command= lambda: self.__define_color(self.__btn_outlines_color, self.__checkvar_outlines))
        self.__btn_outlines_color.grid(column=6, row=1)

        self.__checkvar_center = tk.IntVar()
        self.__checkbtn_center = tk.Checkbutton(self.__frame_out_parameters, text="Centre", variable=self.__checkvar_center, command=lambda: self.__draw_part(self.__btn_center_color, self.__checkvar_center))
        self.__checkbtn_center.grid(column=5, row=2, sticky="W")
        self.__btn_center_color = tk.Button(self.__frame_out_parameters, width = 5, bg="#000", command= lambda: self.__define_color(self.__btn_center_color, self.__checkvar_center))
        self.__btn_center_color.grid(column=6, row=2)

        # Bouton "générer"
        self.__btn_create_img = tk.Button(self.__frame_out_parameters, text="Générer image", command=self.generate, state="disabled")
        self.__btn_create_img.grid(column=0, row=5)
        self.__btn_create_gif = tk.Button(self.__frame_out_parameters, text="Générer GIF animé", command=self.__generate_GIF, state="disabled")
        self.__btn_create_gif.grid(column=1, row=5)
        self.__btn_save = tk.Button(self.__frame_out_parameters, text="Enregistrer sous...", state="disabled", command=self.save)
        self.__btn_save.grid(column=2, row=5)

        self.__frame_img_in = si.ScrollableImageControls(self.__frame_images)
        self.__frame_img_out = si.ScrollableImageControls(self.__frame_images)
        self.__frame_img_in.pack(side="left", fill="both", expand=True)
        self.__frame_img_out.pack(side="right", fill="both", expand=True)

        # Afficher la fenêtre
        self.__root.mainloop()

    def __print_size(self, label, size=None):
        if size:
            label.set("Dimensions : %d x %d" % size)
        else:
            label.set("Dimensions :")

    def __draw_part(self, btn, ckkvar):
        if ckkvar.get() == 1:
            if btn == self.__btn_background_color:
                self.__phenakitiscope.set_color(bg=self.__btn_background_color.cget("bg"))
            elif btn == self.__btn_outlines_color:
                self.__phenakitiscope.set_color(outlines=self.__btn_outlines_color.cget("bg"))
            elif btn == self.__btn_center_color:
                self.__phenakitiscope.set_color(center=self.__btn_center_color.cget("bg"))
        else:
            if btn == self.__btn_background_color:
                self.__phenakitiscope.set_color(bg=None)
            elif btn == self.__btn_outlines_color:
                self.__phenakitiscope.set_color(outlines=None)
            elif btn == self.__btn_center_color:
                self.__phenakitiscope.set_color(center=None)


    def save(self):
        imgs = self.__frame_img_out.get_images()
        if len(imgs) == 1:
            path = tk.filedialog.asksaveasfilename(defaultextension='.png', filetypes=[("Images", '*.png *.jpg *.jpeg')], title="Enregistrer sous...")
            if path:
                ext = pathlib.Path(path).suffix

                img = imgs[0]
                if ext != ".png":
                    img = img.convert('RGB')
                img.save(path)
        else:
            path = tk.filedialog.asksaveasfilename(defaultextension='.gif', filetypes=[("Images", '*.gif')], title="Enregistrer sous...")
            if path:
                imgs[0].save(path, save_all=True, append_images=imgs[1:], optimize=False, duration=round(1000/self.__frame_img_out.get_fps()), loop=0)

    def __define_color(self, btn, ckkvar):
        colorRGB, colorHex = tk.colorchooser.askcolor(color=btn.cget("bg"))
        if colorHex:
            btn.configure(bg=colorHex)
            self.__draw_part(btn, ckkvar)


    def __margin_changed(self, var, indx, mode):
        self.__phenakitiscope.set_margins_bbox(self.__get_margin_values())
        self.__print_size(self.__text_size_out, self.__phenakitiscope.get_size_output())
        
        
    
    def __validate(self, user_input, widget): 
        # check if the input is numeric 
        if  user_input.isdigit(): 
            # Fetching minimum and maximum value of the spinbox 
            minval = int(self.__root.nametowidget(widget).config('from')[4]) 
            maxval = int(self.__root.nametowidget(widget).config('to')[4]) 
    
            # check if the number is within the range 
            if int(user_input) not in range(minval, maxval): 
                return False
    
            # Printing the user input to the console 
            return True
        # if input is blank string 
        elif user_input == "": 
            return True
        # return false is input is not numeric 
        else: 
            return False

    def __onchange_crop(self, event):
        self.__phenakitiscope.crop_values_input_factors(self.__scale_crop_h.get()/100, self.__scale_crop_v.get()/100)
        self.__frame_img_in.set_crop(self.__phenakitiscope.get_crop_bbox())
        self.__print_size(self.__text_size_in, self.__phenakitiscope.get_size_input_cropped())
        self.__print_size(self.__text_size_out, self.__phenakitiscope.get_size_output())
        

    def __show_input_img(self, event=None):
        item_id = self.__tree_files.selection()[0]
        if self.selected_input_img != item_id:
            self.selected_input_img = item_id
            self.__phenakitiscope.crop_values_input_factors(self.__scale_crop_h.get()/100, self.__scale_crop_v.get()/100)
            self.__frame_img_in.set_images([Image.open(self.filenames[int(item_id)])], crop_bbox=self.__phenakitiscope.get_crop_bbox())


    """
    Désactiver tout les widgets pouvant être modifés dans la fenêtre principale
    """
    def __disable_widgets(self):
        self.__btn_browse_files.configure(state="disabled")
        self.__scale_crop_h.configure(state="disabled")
        self.__scale_crop_v.configure(state="disabled")
        self.__btn_create_img.configure(state="disabled")
        self.__btn_create_gif.configure(state="disabled")
        self.__btn_fill.configure(state="disabled")
        self.__btn_insert.configure(state="disabled")

    """
    Activer tout les widgets pouvant être modifés dans la fenêtre principale
    """
    def __enable_widgets(self, generate_btn=True):
        self.__btn_browse_files.configure(state="normal")
        self.__scale_crop_h.configure(state="normal")
        self.__scale_crop_v.configure(state="normal")
        if generate_btn:
            self.__btn_create_img.configure(state="normal")
            self.__btn_create_gif.configure(state="normal")
            self.__btn_fill.configure(state="normal")
            self.__btn_insert.configure(state="normal")

    """
    Action effectuée lors d'un clic sur le bouton "parcourir" :
    Selection des fichiers via une boite de dialogue et vérifications de la validité des images 
    """
    def __select_files(self):
        # par défaut on indique que rien n'est selectionné et on désactive tout les widgets de la fenêtre en attendant la fin du traitement
        self.__stringvar_files_nb.set("Aucune image selectionnée")
        self.__print_size(self.__text_size_in)
        self.__print_size(self.__text_size_out)
        self.__disable_widgets()
        # vider la liste des fichier
        self.__tree_files.delete(*self.__tree_files.get_children())
        self.filenames = []
        self.__phenakitiscope.clear_imgs()
        # effacer l'image
        self.__frame_img_in.clear()
        self.__frame_img_out.clear()
        self.selected_input_img = -1

        self.__btn_save.configure(state="disabled")


        # boîte de dialogue pour la selection des fichiers 
        filenames = filedialog.askopenfilenames(parent=self.__root, title="Selectionnez les fichiers", 
            filetypes = (("Images", "*.jpg *.jpeg *.png"), ("Tous les formats", "*")), multiple=True)
        
        # vérifier qu'il y a au moins 3 images (ce qui est le minimum pour faire fonctionner l'algo correctement)
        if len(filenames) < 3:
            messagebox.showerror(title="Erreur", message="Vous devez selectionnez au minimum 3 images.")
            self.__enable_widgets(False)
            return

        # vérifier que tous les fichiers sont corrects (type de fichier et dimmensions)
        size = None
        for i, f in enumerate(filenames):
            try:
                img_source = Image.open(f)
                if size == None:
                    size = img_source.size
                else:
                    if img_source.size != size:
                        messagebox.showerror(title="Erreur", message="Les images doivent être de même dimensions.")
                        self.__enable_widgets(False)
                        return
            except:
                messagebox.showerror(title="Erreur", message="Ce type de fichier ne peut pas être lus.")
                self.__enable_widgets(False)
                return
            self.__tree_files.insert("", "end", i, text=i, values=(f, "%d x %d" % img_source.size ), tags=("file"))
            self.__phenakitiscope.add_image(img_source)

        
        # garder la liste des fichiers en mémoire et affiché le nombre d'images selectionné
        self.filenames = filenames
        self.__print_size(self.__text_size_in, size)
        self.__stringvar_files_nb.set("%d images selectionnées" % len(filenames))

        # Selectionner la première image
        self.__tree_files.selection_set("0")
        self.__show_input_img()

        self.__print_size(self.__text_size_out, self.__phenakitiscope.get_size_output())

        self.__enable_widgets()

    """
    Remplir automatiquement les valeurs des paramètres de positionement de l'image
    """
    def __set_margin(self, mode):
        if mode == 0:
            self.__spinbox_top.set(0)
            self.__spinbox_left.set(0)
            self.__spinbox_right.set(0)
            self.__spinbox_bottom.set(0)
        else:
            margins = self.__phenakitiscope.calc_margins_bbox_insert()
            self.__spinbox_top.set(margins[0])
            self.__spinbox_left.set(margins[1])
            self.__spinbox_right.set(margins[2])
            self.__spinbox_bottom.set(margins[3])
        self.__print_size(self.__text_size_out, self.__phenakitiscope.get_size_output())

    def generate(self):
        img = self.__phenakitiscope.generate()
        self.__frame_img_out.set_images([img])
        self.__btn_save.configure(state="normal")

    def __generate_GIF(self):
        imgs = self.__phenakitiscope.generate_GIF()
        self.__frame_img_out.set_images(imgs)
        self.__btn_save.configure(state="normal")

    def __get_margin_values(self):
        values = [self.__spinbox_top.get(), self.__spinbox_left.get(), self.__spinbox_right.get(), self.__spinbox_bottom.get()]
        for i, v in enumerate(values):
            try:
                values[i] = int(v)
            except:
                values[i] = 0
        return values


if __name__ == "__main__":
    app = Application()
    app.main()