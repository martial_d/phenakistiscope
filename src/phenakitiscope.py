"""
"""

from PIL import Image, ImageDraw, ImageColor, ImageTk
import math
import copy

"""
"""
class Phénakistiscope():

    def __init__(self, ):
        # initialiser les valeurs de base des attributs
        self.clear_imgs()
        self.__color_background = None
        self.__color_outlines = None
        self.__color_center = None

    """
    Définir la séquence d'image à utiliser.
    imgs : tableau d'images PIL.Image.Image
    """
    def set_images(self, imgs):
        # vérifier que les données en entrée sont correcte
        if not isinstance(imgs, type([])):
            return False
        size = None
        for img in imgs:
            if not isinstance(img, Image.Image):
                return False
            if size == None:
                if img.size[0] <= 0 and img.size[1] <= 0:
                    return False
                size = img.size
            else:
                if size != img.size:
                    return False
        # redéfinir les images de la séquence
        self.clear_imgs()
        self.__imgs_input = imgs
        self.__size_input = imgs[0].size
        self.__nb_img_input += len(self.__imgs_input)
        self.__calc_angle()
        return True
    
    """
    Renvoie la liste des images de la séquence
    """
    def get_images(self):
        return self.__imgs_input
    
    """
    Ajouter une image dans la séquence à la fin
    img : image PIL.Image.Image de la même dimension que les autres images de la séquence
    """
    def add_image(self, img):
        if isinstance(img, Image.Image):
            if img.size[0] > 0 and img.size[1] > 0:
                if self.__nb_img_input == 0:
                    self.__imgs_input.append(img)
                    self.__nb_img_input += 1
                    self.__size_input = img.size
                    self.__size_input_cropped = (img.size[0] - self.__crop_values_input[1] - self.__crop_values_input[2], img.size[1] - self.__crop_values_input[0] - self.__crop_values_input[3])
                    
                    self.__calc_angle()
                    return True
                else:
                    if self.__size_input[0] == img.size[0] and self.__size_input[1] == img.size[1]:
                        self.__imgs_input.append(img)
                        self.__nb_img_input += 1

                        self.__calc_angle()
                        return True
        return False

    """
    Vider la liste d'images
    """
    def clear_imgs(self):
        self.__imgs_input = []
        self.__size_input = (0,0)
        self.__nb_img_input = 0
        self.__size_max_output = 1000
        self.__margin_blank_factor = 0.01

        self.__crop_values_input = [0,0,0,0]
        self.__crop_bbox = [0,0,0,0]
        self.__size_input_cropped = (0,0)
        self.__margins_bbox = [0,0,0,0]

        self.__img_output = None
        self.__size_output = (0,0)
        self.__size_bbox_resized = (0,0)
        self.__size_input_resized = (0,0)
        self.__margins_bbox_resized = [0,0,0,0]
        self.__disc_radius = 0
        self.__size_sector_triangle = (0,0)
        self.__angle_sector_rad = 0


    """
    Renvoie la taille des images de la séquence
    """
    def get_size_input(self):
        return self.__size_input

    """
    Renvoie les valeurs des marges à rogner en pixels par rapport à la taille des images sources [haut, droite, gauche, bas]
    """
    def get_crop_values_input(self):
        return self.__crop_values_input

    """
    Renvoie la boite englobante qui définie la position de l'image rognée dans l'image source
    """
    def get_crop_bbox(self):
        return self.__crop_bbox

    """
    Renvoie la taille de l'image source après avoir été rognée
    """
    def get_size_input_cropped(self):
        return self.__size_input_cropped

    """
    Renvoie la taille des images de la séquence après le redimensionement (s'il y en a un)
    """
    def get_size_input_resized(self):
        return self.__size_input_resized

    """
    Renvoie la taille de l'image finale
    """
    def get_size_output(self):
        return self.__size_output

    """
    Définir les couleurs des différentes parties du disque. Si les valeurs sont "None" alors l'élément ne sera pas dessiné.
    Les couleurs sont au format hexadécimales (exemple : "#000000")
    bg : couleur du fond du disque
    outlines : couleur du contour du disque
    center : couleur du point central
    """
    def set_color(self, bg="", outlines="", center=""):
        if bg != "":
            self.__color_background = bg
        if outlines != "":
            self.__color_outlines = outlines
        if center != "":
            self.__color_center = center

    """
    Définir le rognage des images de la séquence 
    crop_values : tableau contenant les valeurs en pixels des marges à rogner [haut, droite, gauche, bas] 
    """
    def crop_input_imgs(self, crop_values):
        self.__crop_values_input = crop_values
        self.__size_input_cropped = (self.__size_input[0] - self.__crop_values_input[1] - self.__crop_values_input[2], self.__size_input[0] - self.__crop_values_input[0] - self.__crop_values_input[3])
        self.__crop_bbox = (self.__crop_values_input[1], self.__crop_values_input[0], self.__crop_values_input[1] + self.__size_input_cropped[0], self.__crop_values_input[0] + self.__size_input_cropped[1])
        self.__calc_sizes()
        self.__img_output = None

    """
    Définir le rognage des images de la séquence
    fh : coefficent à appliquer en hauteur de 0.0 à 1.0
    fv : coefficent à appliquer en largeur de 0.0 à 1.0
    """
    def crop_values_input_factors(self, fh, fv):
        self.__size_input_cropped = (round(self.__size_input[0]*(1-fh)), round(self.__size_input[1]*(1-fv)))
        h_crop = round((self.__size_input[0]-self.__size_input_cropped[0])/2)
        v_crop = round((self.__size_input[1]-self.__size_input_cropped[1])/2)
        self.__crop_values_input = (v_crop, h_crop, h_crop, v_crop)
        self.__crop_bbox = (self.__crop_values_input[1], self.__crop_values_input[0], self.__crop_values_input[1] + self.__size_input_cropped[0], self.__crop_values_input[0] + self.__size_input_cropped[1])
        self.__calc_sizes()
        self.__img_output = None

    """
    Définir le positionnement des images de la séquence par rapport à la position par défaut.
    Par défaut l'image recouvre entièrement secteur du disque. 
    Cela signifie que soit la hauteur de l'image est égale à la hauteur du secteur 
    soit la largeur est égale à la largeur du secteur ou soit les deux.
    Mais en aucun cas il n'y a de zones vides par défaut. 
    Cette fonction permet donc de définir une nouvelle position en donnant des marges pour les 4 directions.
    values : tableau contenant les valeurs des marges à appliquer
    """
    def set_margins_bbox(self, values):
        self.__margins_bbox = [values[0], values[1], values[2], values[3]]
        self.__calc_sizes()
        self.__img_output = None

    """
    Renvoie les valeurs des marges, en pixel et pour chaque cotés, nécessaire pour avoir l'intégralité des images sources présente dans une section du disque
    """
    def calc_margins_bbox_insert(self):
        # distance entre le centre du disque et le haut de l'image dans le secteur
        distance_from_center = round((self.__size_input_cropped[0]/2) / math.tan(self.__angle_sector_rad/2) ) 
        # hauteur du triangle (formé par les rayons formant les extrémités du secteurs et la corde du cercle) passant par le point au centre du disque
        triangle_height = distance_from_center + self.__size_input_cropped[1]
        # corde du cercle pour le secteur
        triangle_width = round((math.tan(self.__angle_sector_rad/2) * triangle_height)*2)
        # rayon du cercle
        disc_radius = round(math.sqrt(triangle_height*triangle_height + self.__size_input_cropped[0]/2*self.__size_input_cropped[0]/2))

        return [distance_from_center, round((triangle_width - self.__size_input_cropped[0])/2), round((triangle_width - self.__size_input_cropped[0])/2), disc_radius - triangle_height]

    """
    Mettre à jour la valeur de l'angle d'un secteur contenant une image
    """
    def __calc_angle(self):
        # angle de chacun des secteurs du disque final contenant chacun une image de la séquence
        self.__angle_sector_rad = 2*math.pi/self.__nb_img_input
        self.__calc_sizes()
        self.__img_output = None

    """
    Mettre à jour les valeurs des tailles des différents éléments utilisés lors de la génération de l'image
    """
    def __calc_sizes(self):
        # si aucune séquence n'a été spécifiée on renvoie une valeur nulle
        if self.__nb_img_input < 3:
            return None

        # taille de l'image englobante avec les marges permettant d'ajuster la position
        size_box = (
            self.__size_input_cropped[0] + self.__margins_bbox[1] + self.__margins_bbox[2], 
            self.__size_input_cropped[1] + self.__margins_bbox[0] + self.__margins_bbox[3]
        )

        # rayon du disque final
        # par défaut on prend la hauteur de l'image pour tout couvrir en hauteur
        disc_radius = size_box[1]
        # corde du secteur 
        triangle_width = round(math.sin(self.__angle_sector_rad/2)*2*disc_radius)
        # hauteur du tringle inclue dans un secteur du disque 
        triangle_height = round((triangle_width/2)/math.tan(self.__angle_sector_rad/2))

        # si la largeur du secteur est plus grande que l'image 
        # dans ce cas on prend par défaut la largeur du secteur comme étant la largeur de l'image
        if(triangle_width > size_box[0]):
            triangle_width = size_box[0]
            triangle_height = (triangle_width/2)/math.tan(self.__angle_sector_rad/2)
            disc_radius = math.sqrt(triangle_height*triangle_height + (triangle_width/2) * (triangle_width/2))

        # taille de l'image finale en prennant en compte les marges
        size_output = round(disc_radius*2 + (disc_radius*2*self.__margin_blank_factor))

        # calcul du redimensionnement de la taille des images sources si nécessaire
        # pour ne pas dépasser la taille d'image maximale en sortie
        size_bbox_resized = size_box
        size_input_resized = self.__size_input_cropped
        margin_resized = self.__margins_bbox
        if(size_output > self.__size_max_output):
            # ratio hauteur/largeur image
            c = size_box[1]/size_box[0]

            # cas ou rayon = hauteur_image
            if disc_radius == size_box[1]:
                # calculer la nouvelle taille de l'image englobante
                y = round((self.__size_max_output/2) * (1/(1+self.__margin_blank_factor)))
                x = round(y/c)

                # mise à jour des autres valeurs
                disc_radius = y
                triangle_width = round(math.sin(self.__angle_sector_rad/2)*2*disc_radius)
                triangle_height = round((triangle_width/2)/math.tan(self.__angle_sector_rad/2))
            # cas ou largeur_secteur = largeur_image
            else: 
                # calculer la nouvelle taille de l'image englobante
                x = round((self.__size_max_output/2) * (1/(1+self.__margin_blank_factor)) * 2 * math.sin(self.__angle_sector_rad/2) )
                y = round(x*c)

                # mise à jour des autres valeurs
                triangle_width = x
                triangle_height = (triangle_width/2)/math.tan(self.__angle_sector_rad/2)
                disc_radius = math.sqrt(triangle_height*triangle_height + (triangle_width/2) * (triangle_width/2))

            # nouvelles dimmensions
            size_bbox_resized = (x,y)
            factor = x/size_box[0]
            size_input_resized = (round(self.__size_input_cropped[0]*factor), round(self.__size_input_cropped[1]*factor))
            margin_resized = [round(margin_resized[0]*factor), round(margin_resized[1]*factor), round(margin_resized[2]*factor), round(margin_resized[3]*factor)]
            size_output = round(disc_radius*2 + (disc_radius*2*self.__margin_blank_factor))

        self.__size_output = (size_output, size_output)
        self.__size_bbox_resized = size_bbox_resized
        self.__size_input_resized = size_input_resized
        self.__margins_bbox_resized = margin_resized
        self.__disc_radius = disc_radius
        self.__size_sector_triangle = (triangle_width, triangle_height)
        self.__img_output = None


    """
    Créer l'image finale en fonction des paramètres précalculés
    """
    def generate(self):
        # création de l'image finale
        img = Image.new('RGBA', self.__size_output)
        # création d'un objet ImageDraw pour dessiner sur l'image
        draw_img = ImageDraw.Draw(img)

        # tracer le fond du disque 
        top_left = (self.__disc_radius*self.__margin_blank_factor, self.__disc_radius*self.__margin_blank_factor)
        bottom_right = (self.__disc_radius*2+top_left[0], self.__disc_radius*2+top_left[1])
        if self.__color_background:
            draw_img.ellipse((top_left, bottom_right), fill=self.__color_background)

        # créer une image vide pour appliquer le masque (de même taille que l'image englobante)
        mask_composite_bg = Image.new('RGBA', self.__size_bbox_resized)
        # création du masque 
        mask_sector = Image.new('L', self.__size_bbox_resized)
        draw = ImageDraw.Draw(mask_sector)
        draw.pieslice((round(self.__size_bbox_resized[0]/2-self.__disc_radius), -self.__disc_radius, round(self.__size_bbox_resized[0]/2+self.__disc_radius), self.__disc_radius), 90-math.degrees(self.__angle_sector_rad)/2, 90+math.degrees(self.__angle_sector_rad)/2, fill=ImageColor.getcolor("white", "L"))

        # traitement et ajout des images à l'image finale
        for i, img_in in enumerate(self.__imgs_input):
            # rogner l'image
            img_in = img_in.crop(self.__crop_bbox)
            # redimensionner l'image si besoin
            if(self.__size_input_resized != self.__size_input_cropped):
                img_in = img_in.resize(self.__size_input_resized)
            # ajouter les marges
            img_temp = Image.new('RGBA', (self.__size_bbox_resized[0], self.__size_bbox_resized[1]))
            img_temp.paste(img_in, (self.__margins_bbox_resized[1], self.__margins_bbox_resized[0]))
            img_in = img_temp
            # appliquer le masque
            img_temp2 = Image.composite(img_in, mask_composite_bg, mask_sector)
            # créer une nouvelle image vide de la taille de l'image finale pour positionner correctement l'image qui a été modifiée avec le masque
            img_temp3 = Image.new('RGBA', self.__size_output)
            # copier l'image dans la nouvelle image vide de façon a avoir le secteur en bas et au centre de l'image (c'est à dire à la position d'un des secteur du disque)
            img_temp3.alpha_composite(img_temp2, (round(self.__size_output[0]/2-self.__size_sector_triangle[0]/2), round(self.__size_output[1]/2)), (round((self.__size_bbox_resized[0]/2)-(self.__size_sector_triangle[0]/2)), 0))
            # appliquer la rotation par rapport au centre de l'image
            img_temp3 = img_temp3.rotate(-math.degrees(self.__angle_sector_rad)*i, center=(round(self.__size_output[0]/2), round(self.__size_output[1]/2)), expand=False)
            # copier l'image dans l'image finale
            img.alpha_composite(img_temp3, (0, 0), (0, 0))

        # tracer le contour du disque 
        thickness = round(self.__size_output[0]*0.005) if round(self.__size_output[0]*0.005) > 0 else 1
        if self.__color_outlines:
            top_left = (top_left[0]-round(thickness/2), top_left[1]-round(thickness/2))
            bottom_right = (bottom_right[0]+round(thickness/2), bottom_right[1]+round(thickness/2))
            draw_img.ellipse((top_left, bottom_right), outline=self.__color_outlines, width=thickness)

        # tracer le point central
        if self.__color_center: 
            top_left = (round(self.__size_output[0]/2)-thickness, round(self.__size_output[1]/2)-thickness)
            bottom_right = (top_left[0]+thickness*2, top_left[1]+thickness*2)
            draw_img.ellipse((top_left, bottom_right), outline=self.__color_center, fill=self.__color_center, width=thickness)
        self.__img_output = img
        return img

    """
    Créer une séquence d'images permettant de créer une animation du disque
    """
    def generate_GIF(self):
        self.generate()
        img = self.__img_output
        size = self.__img_output.size

        images = []
        for i in range(self.__nb_img_input):
            im = Image.new('RGB', self.__img_output.size)
            # faire la rotation du secteur
            im = img.rotate((360/self.__nb_img_input)*i, center=(round(size[0]/2), round(size[1]/2)), expand=False)
            images.append(im)

        return images