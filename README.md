# Phénakistiscope

Générateur de disques pour phénakistiscope.

Cette application permet de créer des disques à imprimer pour la réalisation de phénakistiscope. 
Les disques sont générés sous la forme d'images. Chaque disque contient une séquence d'images qui représente une animation. Mais il est également possible d'utiliser n'importe quelle série d'images et d'utiliser ce programme à des fins expérimentales et artistiques et créer ainsi des motifs originaux. 

## Prérequis 

* Python 3.9.1
* Pillow 8.1.0

## Installation

### Windows 

Rendez-vous sur la page de téléchargement en [cliquant ici](https://gitlab.com/martial_d/phenakistiscope/-/releases), téléchargez l'archive zip (32 bits ou 64 bits en fonction de votre version de Windows) puis décompressez l'archive. Exécutez ensuite le fichier "phenakistiscope.exe" pour lancer le programme.

### Autres sytèmes d'exploitation

Quel que soit votre sytème d'exploitation vous pouvez installer Python sur votre ordinateur, puis installer le paquet "Pillow".
Il suffit ensuite simplement de télécharger les fichiers de ce dépôt et lancer le programme avec Python. Le point d'entrée du programme est dans le fichier "main.py".

## Utilisation 

## Licence
